import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("1. Thêm loại bánh.\n2. Danh sách bánh.\n3. Thoát.\n Nhập tuỳ chọn:");

            int option = 0;
            try {
                option = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Tuỳ chọn không hợp lệ.");
                continue;
            }

            switch (option) {
                case 1:
                    scanner = new Scanner(System.in);
                    System.out.print("Nhập tên bánh: ");
                    String cakeName = scanner.nextLine();
                    System.out.print("Nhập giá bán: ");
                    double price = scanner.nextDouble();
                    Cake cake = new Cake(cakeName, price);
                    saveCakeToDatabase(cake);
                    System.out.println();
                    break;
                case 2:
                    ArrayList<Cake> cakes = readCakeFromDatabase();
                    for(Cake c : cakes) {
                        System.out.println(c.getName() + " - " + c.getPrice());
                        c.setPrice(c.getPrice() + 5000);
                    }

                    updateCakeToDatabase(cakes);

                    cakes = readCakeFromDatabase();
                    for(Cake c : cakes) {
                        System.out.println(c.getName() + " - " + c.getPrice());
                    }
                    break;
                case 3:
                    return;
                default:
                    break;
            }
        }
    }

    public static ArrayList<Cake> readCakeFromDatabase() {
        ArrayList<Cake> cakes = new ArrayList<>();
        String databaseName = "database.txt";
        File file = new File(databaseName);
        if(file.exists()) {
            try {
                Scanner scannerFile = new Scanner(file);
                while (scannerFile.hasNextLine()) {
                    String lineData = scannerFile.nextLine();
                    String[] elements = lineData.split("\\|");
                    if(elements.length != 2) {
                        continue;
                    }
                    String cakeName = elements[0];
                    double cakePrice = 0;
                    try{
                        cakePrice = Double.parseDouble(elements[1]);
                    }catch (NumberFormatException ex) {

                    }
                    Cake cake = new Cake(cakeName, cakePrice);
                    cakes.add(cake);
                }
                scannerFile.close();
            } catch (FileNotFoundException e) {
                System.out.println("File không tồn tại!");
            }
        }

        return cakes;
    }

    public static void saveCakeToDatabase(Cake cake) {
        String databaseName = "database.txt";
        File file = new File(databaseName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Không thể tạo mới database.");
                return;
            }
        }

        try {
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write("\n" + cake.getName() + " | " + cake.getPrice());
            fileWriter.close();
        } catch (IOException ex) {

        }
    }

    public static void updateCakeToDatabase(ArrayList<Cake> cakes) {
        String databaseName = "database.txt";
        File file = new File(databaseName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Không thể tạo mới database.");
                return;
            }
        }

        try {
            FileWriter fileWriter = new FileWriter(file, false);
            for(Cake cake : cakes) {
                fileWriter.write( cake.getName() + " | " + cake.getPrice() + "\n");
            }
            fileWriter.close();
            System.out.println("Cập nhật giá bán thành công!");
        } catch (IOException ex) {

        }
    }
}